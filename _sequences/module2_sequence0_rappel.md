---
title: "Rappel"
numeroSequence: 0
numeroModule: 2
numeroEdition:
    - 1
type: "cours"
description: "Rappel du module précédent"
temps: 40 minutes

formateurs : 
 - fredericdeLamotte
 - julienSeiler
---

## Un projet sur la durée

{% include repoImage.html pathInRepo="/assets/img/sequences/module2_sequence0/fig1.png" %}

## Cycle de vie des données scientifiques

{% include repoImage.html pathInRepo="/assets/img/sequences/module2_sequence0/fig2.png" %}