# Formation IFB science ouverte & PGD : comment gérer des jeux de données haut-débit en sciences de la vie et de la santé
## Présentation et application des principes FAIR de gestion des données dans un projet bioinformatique.

L’Institut Français de Bioinformatique (IFB) organise une formation à destination de bioinformaticiens, biologistes et médecins impliqués dans des projets d’analyse bioinformatique de jeux de données omiques et souhaitant mettre en œuvre les principes "FAIR" (Facile à trouver, Accessible, Interopérable, Réutilisable) tout au long du déroulement du projet. La formation abordera les différents points fondamentaux (théoriques, pratiques, juridiques) en lien avec la politique nationale d’ouverture des données de la recherche et présentera sous forme de séances pratiques les ressources nationales et internationales accessibles à la communauté scientifique ainsi que les solutions proposées par l’IFB pour gérer les données d’un projet de recherche.

[![](https://img.shields.io/badge/LICENCE-GNU--Affero-brightgreen.svg)](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/blob/main/LICENSE) 

[![Website Ressources](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://ifb-elixirfr.gitlab.io/fair/IFB-FAIR-data-training)

[![made-with-Jekyll](https://img.shields.io/badge/Made%20with-Jekyll-1f425f.svg)](https://jekyllrb.com/)
[![made-with-markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[![made-with-JS](https://img.shields.io/badge/Made%20with-JS-1f425f.svg)](https://developer.mozilla.org/fr/docs/Web/JavaScript)

[![Release](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/latest_release.svg?job=build_badges)]()
[![Commits](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits.svg?job=build_badges)]()
[![Commits since last release](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_since_last_release.svg?job=build_badges)]()

[![Commit activity](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commit_activity.svg?job=build_badges)]()
[![Releases activity](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_activity.svg?job=build_badges)]()
[![Releases](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_amount.svg?job=build_badges)]()
[![Contributors](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/contributors.svg?job=build_badges)]()
[![All Contributors](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/all_contributors.svg?job=build_badges)]()

[![Last release](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/latest_release_date.svg?job=build_badges)]()
[![Last release](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/latest_release_date_layout2.svg?job=build_badges)]()
[![Last commit](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/last_commit_date.svg?job=build_badges)]()
[![Last commit](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/last_commit_date_layout2.svg?job=build_badges)]()

[![Commits per year](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_year.svg?job=build_badges)]()
[![Commits per month](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_month.svg?job=build_badges)]()
[![Commits per day](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_day.svg?job=build_badges)]()
[![Commits per hour](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_hour.svg?job=build_badges)]()
[![Commits per minute](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_minute.svg?job=build_badges)]()
[![Commits per second](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/commits_per_second.svg?job=build_badges)]()

[![Releases per year](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_year.svg?job=build_badges)]()
[![Releases per month](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_month.svg?job=build_badges)]()
[![Releases per day](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_day.svg?job=build_badges)]()
[![Releases per hour](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_hour.svg?job=build_badges)]()
[![Releases per minute](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_minute.svg?job=build_badges)]()
[![Releases per second](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/releases_per_second.svg?job=build_badges)]()

[![Day the Repository has been created](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/jobs/artifacts/main/raw/badges/repository_creation_day.svg?job=build_badges)]()



[:speech_balloon: Posez une question]()
[:book: Lisez les questions]()
[:e-mail: Par mail](mailto:thomas.denecker@gfrance-bioinformatique.fr)

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

### Site de la formation 

Le site de la formation est disponible [:arrow_right: cliquer ici :arrow_left:](https://ifb-elixirfr.gitlab.io/fair/IFB-FAIR-data-training). Vous y trouverez :
- Les cours ;
- Les TPs ; 
- Les formateurs ; 
- Le programme ;
- et bien plus encore ! 

### Ajouter du contenu dans le site de formation

Le site est généré par Jekyll. Il est possible d'y ajouter simplement :
- Des formateurs ;
- Des cours ;
- Des sessions (composées de cours).  

Un `GitLab runner` est actif pour mettre à jour le site lors de changements dans la branche principale (`main`). 

Tout a été fait pour n'avoir aucune programmation à réaliser. En contrepartie, un certain nombre de règles simples sont à respecter. Toutes ces règles sont décrites dans le wiki du site : [ici](https://gitlab.com/ifb-elixirfr/fair/IFB-FAIR-data-training/-/wikis/home)
