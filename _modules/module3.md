---
title: Module 3
header_title: Module 3
header_subTitle: "Les Métadonnées : les standards du domaine des données omiques en biologie et séances pratiques d’annotations de jeux de données"
numeroModule : 3

timeStart: "15-01-2021 09:00"
timeEnd: "15-01-2021 12:00"
timezone: "Europe/Paris"

objectifs:
    - Objectif 1
    - Objectif 2
    - Objectif 3
  
questions:
    - Question 1 
    - Question 2

messages:
    - Message 1
    - Message 2
    - Message 3

homework:
    - Consigne 1
    - Consigne 2
    - Consigne 3
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.
