---
title: Module 1
header_title: Module 1
header_subTitle: Les données de la Recherche et leur centralité dans le processus de recherche
numeroModule : 1

timeStart: "13-01-2021 09:00"
timeEnd: "13-01-2021 12:00"
timezone: "Europe/Paris"

objectifs:
    - Objectif 1
    - Objectif 2
    - Objectif 3
  
questions:
    - Quelle définition pour les données de la recherche ? 
    - Quelles seraient les conditions pour que les données soient (ré)utilisables ?

prerequis:

messages:
    - Message 1
    - Message 2
    - Message 3

homework:
    - Créer un compte sur DMP OPIDoR
    - Créer et ouvrir un Plan de Gestion des Données basé sur le modèle choisi (mode entraînement)
    - Inviter les formateurs comme collaborateurs

fomulaireAvis: "https://forms.gle/XFR84hvTmUvCrzte8"
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.
