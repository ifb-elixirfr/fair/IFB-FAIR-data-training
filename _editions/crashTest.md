---
title: "Crash test"
description: "Cette édition a pour objectif de tester le contenu pédagogique sur un public ciblé"
numeroEdition : 1

timeStart: "13-01-2021 09:00"
timeEnd: "21-01-2021 12:00"
timezone: "Europe/Paris"

---
## Informations générales de l'édition

### Localisation

Chez nous !

## Les participants

## Les créations lors de l'édition
